from django.urls import path
from home import views

urlpatterns = [
    path('page/', views.homepage, name='home'),
    path('all_students/', views.home, name='List_of_students'),
    path('cars/', views.cars, name='list_of_cars'),
    path('', views.HomeTemplateView.as_view(), name='homepage')
]
