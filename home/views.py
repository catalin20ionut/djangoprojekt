from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def homepage(request):
    return HttpResponse('Hello, Cătălin')


@login_required
def home(request):
    context = {
        "all_students": [
            {'first_name': 'Claudiu',
             'last_name': 'Nechifor',
             'age': 31
             },
            {'first_name': 'Mircea',
             'last_name': 'Popovici',
             'age': 42
             }
        ]
    }
    return render(request, 'home/home.html', context)


def cars(request):
    context = {
        "cars": [
            {'name': 'Ferrari',
             'model': 'Stradale',
             'year_of_fabrication': 2019
             },
            {'name': 'Maserati',
             'model': 'MC20',
             'year_of_fabrication': 2020
             }
        ]
    }
    return render(request, 'home/cars.html', context)


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'
