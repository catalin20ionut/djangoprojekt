import bs4
import requests
import xlsxwriter

url = 'https://www.evomag.ro/portabile-laptopuri-notebook/'
result = requests.get(url)
soup = bs4.BeautifulSoup(result.text, 'lxml')
cases = soup.find_all('div', class_="nice_product_item")
context = {'data': []}
for case in cases:
    data = {}
    product_name = case.find('div', class_="npi_name")
    #print(product_name.text)
    product_price = case.find('span', class_="real_price")

    data['product_name'] = product_name.text
    data['product_price'] = product_price.text
    context['data'].append(data)

workbook = xlsxwriter.Workbook('laptops.xlsx')
worksheet = workbook.add_worksheet('Laptops')

row = 0
col = 0
for i in context['data']:
    worksheet.write(row, col, i['product_name'])
    worksheet.write(row, col + 1, i['product_price'])
    row += 1

workbook.close()
