from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import TextInput


class UserExtendForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please insert your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please insert your last name', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Please insert your email', 'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Please insert your username', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(UserExtendForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'Please enter your password'
        self.fields['password2'].widget.attrs['placeholder'] = 'Please confirm your password'
        # self.fields['password1'].label = 'Parola'
