from django.urls import path
from userextend import views

urlpatterns = [
    path('create-user/', views.CreateUser.as_view(), name='create-user')
]
