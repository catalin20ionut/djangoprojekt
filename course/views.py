from django.shortcuts import render
from teacher.models import Teacher


def get_teachers_per_course(request, teacher_id):
    all_teachers_per_course = Teacher.objects.filter(id=teacher_id)
    context = {'get_all_teachers': all_teachers_per_course}
    return render(request, 'course/get_all_teachers_per_course.html', context)
