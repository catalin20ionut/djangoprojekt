from django.urls import path
from course import views

urlpatterns = [
    path('teachers_per_course/<int:teacher_id>/', views.get_teachers_per_course, name='teachers-per-course')

]
