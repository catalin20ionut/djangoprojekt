from django.urls import path, include
from rest_framework import routers
from teacher import views
from teacher.views import TeacherViewSet

router = routers.DefaultRouter()
router.register('api-teacher', TeacherViewSet)

urlpatterns = [
    path('create_teacher/', views.TeacherCreateView.as_view(), name='create-teacher'),
    path('list_of_teacher/', views.TeacherListView.as_view(), name='list-of-teachers'),
    path('update_teacher/<int:pk>/', views.TeacherUpdateView.as_view(), name='update-teacher'),
    path('delete_teacher/<int:pk>/', views.TeacherDeleteView.as_view(), name='delete-teacher'),
    path('detail_teacher/<int:pk>/', views.TeacherDetailView.as_view(), name='detail-teacher'),
    path('students_per_teacher/<int:id_teacher>/', views.get_all_students_per_teacher, name='students-per-teacher'),
    path('', include(router.urls))
]
