# Generated by Django 3.2.9 on 2021-11-23 08:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='active',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='specialization',
            field=models.CharField(choices=[('Geography', 'Geography'), ('Math', 'Math'), ('History', 'History')], max_length=30),
        ),
    ]
