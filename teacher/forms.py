from django import forms
from django.forms import TextInput, Select
from teacher.models import Teacher


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name', 'specialization', 'active']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please, enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please, enter your last name', 'class': 'form-control'}),
            'specialization': Select(attrs={'class': 'form-control'})
        }
