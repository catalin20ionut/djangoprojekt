from django import forms
from django.forms import TextInput
from django.forms.widgets import Select

from student.models import Student


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'age', 'date_of_birth', 'olympic', 'gender', 'teacher', 'course']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please, enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please, enter your last name', 'class': 'form-control'}),
            'age': TextInput(attrs={'placeholder': 'Please, enter your age', 'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'gender': Select(attrs={'class': 'form-control'}),
            'teacher': Select(attrs={'class': 'form-control'}),
            'course': Select(attrs={'class': 'form-control'}),
        }

# if we want to change first_name with Prenume
#     def __init__(self, *args, **kwargs):
#         super(StudentForm, self).__init__(*args, **kwargs)
#         self.fields['first_name'].label = 'Prenume'
