import django_filters

from student.models import Student


class StudentFilters(django_filters.FilterSet):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name']
