from django.urls import path, include
from rest_framework import routers

from student import views
from student.views import StudentViewSet

router = routers.DefaultRouter()  # it helps us to see the data we send in API
router.register('api-student', StudentViewSet)  # we register our API with the class StudentViewSet
# api-student is the prefix of the url


urlpatterns = [
    path('create_student/', views.StudentCreateView.as_view(), name='create-student'),
    path('list_of_student/', views.StudentListView.as_view(), name='list-of-students'),
    path('update_student/<int:pk>/', views.StudentUpdateView.as_view(), name='update-student'),
    path('delete_student/<int:pk>/', views.StudentDeleteView.as_view(), name='delete-student'),
    path('detail_student/<int:pk>/', views.StudentDetailView.as_view(), name='detail-student'),
    path('', include(router.urls))
]
