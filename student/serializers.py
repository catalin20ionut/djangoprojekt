from rest_framework import serializers

from student.models import Student


class StudentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student  # models represents the table which we get the data
        fields = ['first_name', 'last_name', 'age', 'date_of_birth']


""" it helps that the data specified by the developer be sent. """
